const Discord = require('discord.js');
const config = require('./config.json');
const bot = new Discord.Client();
bot.login(config.bot.token);

bot.on('ready', () => {
    require('./core/events/ready')(bot);
});

bot.on('message', (message) => {
    if (message.author.id !== '406786366295375872' && message.channel.type === 'text' /*&& message.guild.id == '336868616022654986'*/) {
        try {
            require('./core/events/message')(bot, message);
            require('./core/functions/endpoints/users/fetch')(bot, message);
        } catch (err) {
            bot.fetchUser('320933389513523220').then(user => {
                const embed = new Discord.RichEmbed()
                    .setAuthor('DiscordRPG Helper Error', bot.user.avatarURL)
                    .setTitle('Error')
                    .setDescription(`An error occured.\`\`\`${err}\`\`\``)
                    .setColor('RED')
                    .setFooter(`${message.guild.name} / #${message.channel.name} / ${message.author.username}`, message.author.avatarURL);
                user.send({embed});
            })
        }
    }
});