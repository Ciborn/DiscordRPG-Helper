const request = require('request');
const config = require('./../../config.json');
module.exports = function(url, callback) {
    var options = {
        url: url,
        headers: {
          'Authorization': config.keys.discorddungeons
        }
    }

    try {
        request(options, function(err, response, body) {
            if (err) {
                console.log(`DiscordDungeons API Call |  ERROR  | ${url}`)
            } else {
                callback(err, body);
                console.log(`DiscordDungeons API Call | SUCCESS | ${url}`)
            }
        })
    } catch (err) {
        throw err;
    }
}