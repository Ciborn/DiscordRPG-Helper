// database/poolQuery.js

const pool = require('./returnPool.js');
module.exports = function(query, callback) {
    try {
        pool.getConnection(function(err, connection) {
            if (err) {
                connection.release();
                callback(err, null);
            } else {
                connection.query(query, function(err, result) {
                    connection.release();
                    callback(err, result);
                });
            }
        });
    } catch (err) {
        throw err;
    }
}