// Afficher une erreur

const Discord = require('discord.js');
module.exports = function(bot, title, description, message) {
    var embed = new Discord.RichEmbed()
        .setAuthor('DiscordRPG Helper', bot.user.avatarURL)
        .setTitle(title)
        .setDescription(description)
        .setColor(`RED`);
    message.channel.send({embed});
}