const Discord = require('discord.js');
const apicall = require('./../../functions/apicall');
const numeral = require('numeral');
module.exports = function(bot, message, players, guildId, type) {
    apicall(`http://api.discorddungeons.me/v3/guild/${guildId}`, function(err, body) {
        const drpg = JSON.parse(body);

        var typeName = type;
        var prefixSuffix = '%s';
        if (type == 'level') {
            typeName = 'Character Level';
            prefixSuffix = 'Lv%s';
        } else if (type == 'skillsPoints') {
            typeName = 'Total Character Skills Points';
            prefixSuffix = '%s pts';
        } else if (type == 'petLevel') {
            typeName = 'Pet Level';
            prefixSuffix = 'Lv%s';
        } else if (type == 'kills') {
            typeName = 'Kills';
            prefixSuffix = '%s kills';
        } else if (type == 'gold') {
            typeName = 'Gold';
            prefixSuffix = '%s Gold';
        } else if (type == 'playerkills') {
            typeName = 'Player Kills';
            prefixSuffix = '%s kills';
        } else if (type == 'xp') {
            typeName = 'Character Experience Points';
            prefixSuffix = '%s XP';
        } else if (type == 'crits') {
            typeName = 'Attributes Points in Criticals';
            prefixSuffix = '%s pts';
        } else if (type == 'defense') {
            typeName = 'Attributes Points in Defense';
            prefixSuffix = '%s pts';
        } else if (type == 'goldBoost') {
            typeName = 'Attributes Points in Gold Boost';
            prefixSuffix = '%s pts';
        } else if (type == 'lumberBoost') {
            typeName = 'Attributes Points in Lumber Boost';
            prefixSuffix = '%s pts';
        } else if (type == 'mineBoost') {
            typeName = 'Attributes Points in Mine Boost';
            prefixSuffix = '%s pts';
        } else if (type == 'chop') {
            typeName = 'Skills Points in Chopping';
            prefixSuffix = '%s pts';
        } else if (type == 'mine') {
            typeName = 'Skills Points in Mining';
            prefixSuffix = '%s pts';
        } else if (type == 'fish') {
            typeName = 'Skills Points in Fishing';
            prefixSuffix = '%s pts';
        } else if (type == 'forage') {
            typeName = 'Skills Points in Foraging';
            prefixSuffix = '%s pts';
        } else if (type == 'reaping') {
            typeName = 'Attributes Points in Reaping';
            prefixSuffix = '%s pts';
        } else if (type == 'salvaging') {
            typeName = 'Attributes Points in Salvaging';
            prefixSuffix = '%s pts';
        } else if (type == 'scavenging') {
            typeName = 'Attributes Points in Scavenging';
            prefixSuffix = '%s pts';
        } else if (type == 'taming') {
            typeName = 'Attributes Points in Taming';
            prefixSuffix = '%s pts';
        } else if (type == 'xpboost') {
            typeName = 'Attributes Points in XP Boost';
            prefixSuffix = '%s pts';
        } else if (type == 'strength') {
            typeName = 'Attributes Points in Strength';
            prefixSuffix = '%s pts';
        }

        var embed = new Discord.RichEmbed()
            .setTitle(`${drpg.name} Leaderboard | ${typeName}`)
            .setColor(drpg.displayColor);

        if (drpg.tag) {
            embed.setAuthor(`${drpg.tag} ${drpg.name}`)
        } else {
            embed.setAuthor(`${drpg.name}`)
        }
        
        players[Symbol.iterator] = function* () {
            yield* [...this.entries()].sort((a, b) => b[1] - a[1]);
        }
        
        var number = 1;
        var maxPlayers = 12;
        var badge = '';
        var average = 0;
        for (let [key, value] of players) {
            average += value;
            if (number < maxPlayers+1) {
                if (number == 1) {
                    badge = ':first_place:';
                } else if (number == 2) {
                    badge = ':second_place:';
                } else if (number == 3) {
                    badge = ':third_place:';
                } else {
                    badge = `#` + String(number);
                }
                embed.addField(`${badge} - ${prefixSuffix.replace('%s', numeral(value).format('0,0'))}`, key, true);
                number += 1;
            }
        }

        embed.setDescription(`Some people or you can not be on the database because you/your pet has an invalid name, or never talked in a server where the bot is.\n**Average ${typeName}** : ${numeral(Math.floor(average/players.size)).format('0,0')}`)
        message.channel.send({embed});
    })
}