const poolQuery = require('./../../database/poolQuery');
const apicall = require('./../../apicall');
const isEmpty = require('./../../isEmpty');
const Discord = require('discord.js');
module.exports = function(bot, message, guildId) {
    const sendError = function(err) {
        bot.fetchUser('320933389513523220').then(user => {
            const embed = new Discord.RichEmbed()
                .setAuthor('DiscordRPG Helper Error', bot.user.avatarURL)
                .setTitle('BDD Error')
                .setDescription(`An error occured.\`\`\`${err}\`\`\``)
                .setColor('RED')
                .setFooter(`${message.guild.name} / #${message.channel.name} / ${message.author.username}`, message.author.avatarURL);
            user.send({embed});
        })
    }
    poolQuery(`SELECT * FROM guilds WHERE guildId='${guildId}'`, function(err, result) {
        if (err) {
            sendError(err);
        } else {
            if (isEmpty(result)) {
                apicall(`http://api.discorddungeons.me/v3/guild/${guildId}`, function(err, body) {
                    result = JSON.parse(body);
                    poolQuery(`INSERT INTO guilds (guildId, name, guildData, lastUpdated) VALUES ('${guildId}', '${result.name}', '${body}', ${new Date().getTime()})`, function(err, result) {
                        if (err) {
                            sendError(err);
                        }
                    })
                })
            } else {
                if (result[0].lastUpdated+3600000 <= new Date().getTime()) {
                    apicall(`http://api.discorddungeons.me/v3/guild/${guildId}`, function(err, body) {
                        result = JSON.parse(body);
                        poolQuery(`UPDATE guilds SET name='${result.name}', guildData='${body}', lastUpdated='${new Date().getTime()}' WHERE guildId='${guildId}'`, function(err, result) {
                            if (err) {
                                sendError(err);
                            }
                        })
                    })
                }
            }
        }
    })
}