const apicall = require('./../../apicall');
const Discord = require('discord.js');
const guildFetch = require('./../guilds/fetch');
const numeral = require('numeral');
module.exports = function(bot, message) {
    var userId = message.author.id;
    if (message.mentions.members.size != 0) {
        userId = message.mentions.members.first().user.id;
    } else if (message.content.length != 6) {
        const args = message.content.slice(1).trim().split(/ +/g);
        const command = args.shift().toLowerCase();
        if (args[0].length == 17 || args[0].length == 18) {
            userId = args[0];
        }
    }
    apicall(`http://api.discorddungeons.me/v3/user/${userId}`, function(err, body) {
        if (err || JSON.parse(body).error == 'Not Found') {
            const embed = new Discord.RichEmbed()
                .setAuthor(bot.user.username, bot.user.avatarURL)
                .setTitle('API Error')
                .setDescription(`An unknown error occured when calling API.\n\`\`\`xl\n${err}\`\`\``)
                .setColor('RED')
                .setFooter(message.author.username, message.author.avatarURL);
            message.channel.send({embed});
        } else {
            const result = JSON.parse(body);
            bot.fetchUser(userId).then(user => {
                var embed = new Discord.RichEmbed()
                    .setAuthor(user.username, user.avatarURL)
                    .setTitle(`DiscordRPG Stats of ${user.username}`)
                    .addField('Health', `${numeral(result.hp).format('0,0')} HP / ${numeral(result.maxhp).format('0,0')} HP`, true)
                    .addField('Level', result.level, true)
                    .addField('Experience', `${numeral(result.xp).format('0,0')} XP`, true)
                    .setColor('BLUE')
                    .addField('Gold', `${numeral(result.gold).format('0,0')} Gold`, true);

                if (result.guild != '') {
                    embed.addField('Guild', result.guild, true);
                }
                if (message.author.id != result.id) {
                    embed.setFooter(message.author.username, message.author.avatarURL);
                }
                message.channel.send({embed});
            });
        }
    })
}