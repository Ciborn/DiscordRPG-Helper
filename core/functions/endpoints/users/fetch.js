const poolQuery = require('./../../database/poolQuery');
const Discord = require('discord.js');
const errorEmbed = require('./../../errorEmbed');
const apicall = require('./../../apicall');
const isEmpty = require('./../../isEmpty');
const sqlString  = require('sqlstring');
const util = require('util');
module.exports = function(bot, message) {
    if (message.author.bot == false) {
        poolQuery(`SELECT * FROM players WHERE userId='${message.author.id}'`, function(err, result) {
            if (err) {
                bot.fetchUser('320933389513523220').then(user => {
                    const embed = new Discord.RichEmbed()
                        .setAuthor('DiscordRPG Helper Error', bot.user.avatarURL)
                        .setTitle('BDD Error')
                        .setDescription(`An error occured.\`\`\`${err}\`\`\``)
                        .setColor('RED')
                        .setFooter(`${message.guild.name} / #${message.channel.name} / ${message.author.username}`, message.author.avatarURL);
                    user.send({embed});
                })
            } else {
                if (!isEmpty(result)) {
                    if (result[0].lastUpdated+600000 <= new Date().getTime()) {
                        apicall(`http://api.discorddungeons.me/v3/user/${message.author.id}`, function(err, body) {
                            result = JSON.parse(body);
                            poolQuery(`UPDATE players SET username='${message.author.username}', guildId='${result.guild}', playerData='${body}', lastUpdated=${new Date().getTime()} WHERE userId='${message.author.id}'`, function(err, result) {
                                result = JSON.parse(body);
                                if (err) {
                                    poolQuery(`UPDATE players SET username='eyes', guildId='${result.guild}', playerData='eyes', lastUpdated=${new Date().getTime()+25200000} WHERE userId='${message.author.id}'`, function(err, result) {
                                        result = JSON.parse(body);
                                        if (err) {
                                            bot.fetchUser('320933389513523220').then(user => {
                                                const embed = new Discord.RichEmbed()
                                                    .setAuthor('DiscordRPG Helper Error', bot.user.avatarURL)
                                                    .setTitle('BDD Error')
                                                    .setDescription(`An error occured.\`\`\`${err}\`\`\``)
                                                    .setColor('RED')
                                                    .setFooter(`${message.guild.name} / #${message.channel.name} / ${message.author.username}`, message.author.avatarURL);
                                                user.send({embed});
                                            })
                                        }
                                    })
                                }
                                return result;
                            })
                        })
                    } else {
                        return result.playerData;
                    }
                } else {
                    apicall(`http://api.discorddungeons.me/v3/user/${message.author.id}`, function(err, body) {
                        const response = JSON.parse(body);
                        if (err) {
                            bot.fetchUser('320933389513523220').then(user => {
                                const embed = new Discord.RichEmbed()
                                    .setAuthor('DiscordRPG Helper Error', bot.user.avatarURL)
                                    .setTitle('API Error')
                                    .setDescription(`An error occured.\`\`\`${err}\`\`\``)
                                    .setColor('RED')
                                    .setFooter(`${message.guild.name} / #${message.channel.name} / ${message.author.username}`, message.author.avatarURL);
                                user.send({embed});
                            })
                        } else {
                            poolQuery(`INSERT INTO players (userId, username, guildId, playerData, lastUpdated) VALUES ('${message.author.id}', '${message.author.username}', '${response.guild}', '${body}', ${new Date().getTime()})`, function(err, result) {
                                if (err) {
                                    poolQuery(`INSERT INTO players (userId, username, guildId, playerData, lastUpdated) VALUES ('${message.author.id}', 'eyes', '${response.guild}', 'eyes', ${new Date().getTime()+25200000})`, function(err, result) {
                                        if (err) {
                                            bot.fetchUser('320933389513523220').then(user => {
                                                const embed = new Discord.RichEmbed()
                                                    .setAuthor('DiscordRPG Helper Error', bot.user.avatarURL)
                                                    .setTitle('API Error')
                                                    .setDescription(`An error occured.\nAlso, this error may spam the API, so take care of this.\`\`\`${err}\`\`\``)
                                                    .setColor('RED')
                                                    .setFooter(`${message.guild.name} / #${message.channel.name} / ${message.author.username}`, message.author.avatarURL);
                                                user.send({embed});
                                            })
                                        }
                                    })
                                }
                            })
                        }
                        return response;
                    })
                }
            }
        })
    }
}