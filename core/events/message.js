const config = require('./../../config.json');
module.exports = function(bot, message) {
    const prefix = config.bot.prefix;
    const args = message.content.slice(prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();
    if (message.content.indexOf(`${prefix}ping`) == 0) {
        require('./../commands/ping')(bot, message);
    } else if (message.content.indexOf(`${prefix}eval`) == 0) {
        require('./../commands/eval')(bot, message);
    } else if (message.content.indexOf(`${prefix}query`) == 0) {
        require('./../commands/query')(bot, message);
    } else if (message.content.indexOf(`${prefix}stats`) == 0) {
        require('./../functions/endpoints/users/display')(bot, message);
    } else if (message.content.indexOf(`${prefix}tinfo`) == 0) {
        require('./../commands/trade')(bot, message, args);
    } else if (message.content.indexOf(`${prefix}lead`) == 0) {
        require('./../commands/lead')(bot, message, args);
    } else if (message.content.indexOf(`${prefix}help lead`) == 0) {
        require('./../commands/help/lead')(bot, message);
    } else if (message.content.indexOf(`${prefix}recipe`) == 0) {
        require('./../commands/recipe')(bot, message, args);
    } else if (message.content.indexOf(`${prefix}fetchTrades`) == 0) {
        require('./../commands/fetchTrades')(bot, message, args);
    }
}