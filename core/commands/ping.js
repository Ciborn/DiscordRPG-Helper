// Ping command

const Discord = require('discord.js');
module.exports = function(bot, message) {
    const embed = new Discord.RichEmbed()
        .setAuthor('DiscordRPG Helper', bot.user.avatarURL)
        .addField('WebSocket Heartbeat', `${Math.floor(bot.ping)} ms`, true)
        .addField('Host Ping', `Computing...`, true)
        .setColor('BLUE');
    message.channel.send({embed}).then(function(newMessage) {
        const ping = newMessage.createdTimestamp - message.createdTimestamp;
        const embed = new Discord.RichEmbed()
            .setAuthor('DiscordRPG Helper', bot.user.avatarURL)
            .addField('WebSocket Heartbeat', `${Math.floor(bot.ping)} ms`, true)
            .addField('Host Ping', `${ping} ms`, true)
            .setColor('BLUE');
        newMessage.edit({embed});
    }).catch(function(error) {
        const embed = new Discord.RichEmbed()
            .setAuthor('DiscordRPG Helper', bot.user.avatarURL)
            .setTitle('An error occured')
            .setDescription(`Please report this to <@320933389513523220> (**Ciborn**).\n\`\`\`${error}\`\`\``)
            .setColor('RED');
        message.channel.send({embed});
    })
}