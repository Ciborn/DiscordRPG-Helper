const isEmpty = require('./../functions/isEmpty');
const apicall = require('./../functions/apicall');
const fetchPlayer = require('./../functions/endpoints/users/fetch');
const convertDate = require('./../functions/convertDate');
const Discord = require('discord.js');
const numeral = require('numeral');
module.exports = function(bot, message, args) {
    if (!isEmpty(args)) {
        apicall(`http://api.discorddungeons.me/v3/trade/${args[0]}`, function(err, body) {
            const result = JSON.parse(body)
            if (err) {
                const embed = new Discord.RichEmbed()
                    .setAuthor('DiscordRPG Helper Error', bot.user.avatarURL)
                    .setTitle('API Error - /v3/trade')
                    .setDescription(`An error occured. Please report this to <@320933389513523220> (**Ciborn**).\`\`\`${err}\`\`\``)
                    .setColor('RED')
                    .setFooter(`${message.guild.name} / #${message.channel.name} / ${message.author.username}`, message.author.avatarURL);
                message.channel.send({embed});
            } else {
                try {
                    bot.fetchUser(result.from).then(user => {
                        var item = 0;
                        var title = '';
                        var quantity1 = 0;
                        var quantity2 = 0;
                        var price = 0;
                        if (result.type == 'sell') {
                            quantity1 = Object.values(result.sell.item)[0];
                            quantity2 = result.sold;
                            price = result.sell.price;
                            item = parseInt(Object.keys(result.sell.item)[0]);
                            title = 'Items Sold';
                        } else {
                            quantity1 = Object.values(result.buy.item)[0];
                            quantity2 = result.bought;
                            price = result.buy.price;
                            item = parseInt(Object.keys(result.buy.item)[0]);
                            title = 'Items Bought';
                        }
                        apicall(`http://api.discorddungeons.me/v3/item/${item}`, function(err, bodyItem) {
                            const resultItem = JSON.parse(bodyItem);
                            const embed = new Discord.RichEmbed()
                                .setAuthor(user.username, user.avatarURL)
                                .setTitle(`${args[0]} trade details`)
                                .setDescription(`Item ID : ${item} | **${resultItem.name}**`)
                                .addField(title, `${quantity2} / ${quantity1}`, true)
                                .addField(`Price for each Item`, `${numeral(price).format('0,0')} Gold`, true)
                                .setThumbnail(`http://res.discorddungeons.me/images/${resultItem.image}`)
                                .setColor('GREEN')
                                .setTimestamp(new Date(result.created))
                            message.channel.send({embed});
                        })
                    })
                } catch (err) {
                    const embed = new Discord.RichEmbed()
                        .setAuthor('DiscordRPG Helper Error', bot.user.avatarURL)
                        .setTitle('API Error - /v3/trade')
                        .setDescription(`An error occured. Please report this to <@320933389513523220> (**Ciborn**).\`\`\`${err}\`\`\``)
                        .setColor('RED')
                        .setFooter(`${message.guild.name} / #${message.channel.name} / ${message.author.username}`, message.author.avatarURL);
                    message.channel.send({embed});
                }
            }
        })
    }
}