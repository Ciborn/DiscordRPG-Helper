// eval

const Discord = require('discord.js');
module.exports = function(bot, message) {
    if (message.author.id == '320933389513523220') {
        try {
            const messageContentLengthReduced = message.content.length - 6;
            const code = message.content.substr(6, messageContentLengthReduced);
            let evaled = eval(code);
            
            if (typeof evaled !== "string") {
                evaled = require("util").inspect(evaled);
            }
            
            message.channel.send(evaled, {code:"xl"});
        } catch (err) {
            message.channel.send(`An error occured\n\`\`\`xl\n${err}\n\`\`\``);
        }
    } else {
        const embed = new Discord.RichEmbed()
            .setAuthor('DiscordRPG Helper', bot.user.avatarURL)
            .setTitle('Developer Command')
            .setDescription(`Sorry, but you are not not verified as a developer of this bot. Only **Ciborn** can use this command.`)
            .setColor(`RED`)
            .setFooter(message.author.username, message.author.avatarURL);
        message.channel.send({embed});
    }
}