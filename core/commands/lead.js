const poolQuery = require('./../functions/database/poolQuery');
const sendEmbed = require('./../functions/leaderboard/sendEmbed')
const isEmpty = require('./../functions/isEmpty');
module.exports = function(bot, message, args) {
    var type = 'level';
    if (!isEmpty(args)) {
        type = args[0];
    }
    const sendError = function(err) {
        bot.fetchUser('320933389513523220').then(user => {
            const embed = new Discord.RichEmbed()
                .setAuthor('DiscordRPG Helper Error', bot.user.avatarURL)
                .setTitle('BDD Error')
                .setDescription(`An error occured.\`\`\`${err}\`\`\``)
                .setColor('RED')
                .setFooter(`${message.guild.name} / #${message.channel.name} / ${message.author.username}`, message.author.avatarURL);
            user.send({embed});
        })
    }
    poolQuery(`SELECT guildId FROM players WHERE userId='${message.author.id}'`, function(err, guildId) {
        if (err) {
            sendError(err);
        } else {
            if (guildId[0] == '') {
                message.channel.send(`It seems that you are not in a guild, **${message.author.username}**, so I can't now for what guild you are looking.`);
            } else {
                poolQuery(`SELECT * FROM players WHERE guildId='${guildId[0].guildId}'`, function(err, result) {
                    var playersLevels = new Map();
                    var average = 0;
                    var numberOfPlayers = 0;
                    result.forEach(player => {
                        if (player.username != 'eyes') {
                            if (type.indexOf('l') == 0) {
                                if (args[1] != 'avg') {
                                    playersLevels.set(player.username, JSON.parse(player.playerData).level);
                                    type = 'level';
                                } else {
                                    average += JSON.parse(player.playerData).level;
                                    numberOfPlayers++;
                                }
                            } else if (type.indexOf('st') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).attributes.strength);
                                type = 'strength';
                            } else if (type.indexOf('sa') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).attributes.salvaging);
                                type = 'salvaging';
                            } else if (type.indexOf('sc') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).attributes.scavenge);
                                type = 'scavenging';
                            } else if (type.indexOf('s') == 0) {
                                const skillsPoints = JSON.parse(player.playerData).skills.chop.xp + JSON.parse(player.playerData).skills.fish.xp + JSON.parse(player.playerData).skills.forage.xp + JSON.parse(player.playerData).skills.mine.xp;
                                playersLevels.set(player.username, skillsPoints);
                                type = 'skillsPoints';
                            } else if (type.indexOf('pl') == 0 || type == 'pk') {
                                playersLevels.set(player.username, JSON.parse(player.playerData).playerkills);
                                type = 'playerkills';
                            } else if (type.indexOf('p') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).pet.level);
                                type = 'petLevel';
                            } else if (type.indexOf('k') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).kills);
                                type = 'kills';
                            } else if (type.indexOf('goldb') == 0 || type == 'gb') {
                                playersLevels.set(player.username, JSON.parse(player.playerData).attributes.goldBoost);
                                type = 'goldBoost';
                            } else if (type.indexOf('g') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).gold);
                                type = 'gold';
                            } else if (type.indexOf('xpb') == 0 || type == 'xb') {
                                playersLevels.set(player.username, JSON.parse(player.playerData).attributes.xpBoost);
                                type = 'xpboost';
                            } else if (type.indexOf('x') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).xp);
                                type = 'xp';
                            } else if (type.indexOf('cr') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).attributes.crits);
                                type = 'crits';
                            } else if (type.indexOf('d') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).attributes.defense);
                                type = 'defense';
                            } else if (type.indexOf('l') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).attributes.lumberBoost);
                                type = 'lumberBoost';
                            } else if (type.indexOf('mineb') == 0 || type == 'mb') {
                                playersLevels.set(player.username, JSON.parse(player.playerData).attributes.mineBoost);
                                type = 'mineBoost';
                            } else if (type.indexOf('ch') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).skills.chop.xp);
                                type = 'chop';
                            } else if (type.indexOf('m') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).skills.mine.xp);
                                type = 'mine';
                            } else if (type.indexOf('fi') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).skills.fish.xp);
                                type = 'fish';
                            } else if (type.indexOf('fo') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).skills.forage.xp);
                                type = 'forage';
                            } else if (type.indexOf('r') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).attributes.reaping);
                                type = 'reaping';
                            } else if (type.indexOf('t') == 0) {
                                playersLevels.set(player.username, JSON.parse(player.playerData).attributes.taming);
                                type = 'taming';
                            } else {
                                playersLevels.set(player.username, JSON.parse(player.playerData).level);
                                type = 'level';
                            }
                        }
                    })
                    if (average != 0) {
                        message.channel.send(average/numberOfPlayers);
                    } else {
                        sendEmbed(bot, message, playersLevels, guildId[0].guildId, type);
                    }
                })
            }
        }
    })
}