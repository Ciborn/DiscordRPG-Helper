const Discord = require('discord.js');
const prefix = require('./../../../config.json').bot.prefix;
module.exports = function(bot, message) {
    const embed = new Discord.RichEmbed()
        .setAuthor(bot.user.username, bot.user.avatarURL)
        .setTitle(`Help Page for Leaderboard command`)
        .addField('Stats', `**${prefix}lead l** : Character Level\n**${prefix}lead s** : Total Character Skills Points\n**${prefix}lead x** : Character Experience Points\n**${prefix}lead pk** : Player Kills\n**${prefix}lead k** : Kills\n**${prefix}lead g** : Gold`)
        .addField(`Attributes`, `**${prefix}lead xb** : Attributes Points in XP Boost\n**${prefix}lead gb** : Attributes Points in Gold Boost\n**${prefix}lead mb** : Attributes Points in Mine Boost\n**${prefix}lead lb** : Attributes Points in Lumber Boost\n**${prefix}lead st** : Attributes Points in Strength\n**${prefix}lead cr** : Attributes Points in Criticals\n**${prefix}lead d** : Attributes Points in Defense\n**${prefix}lead r** : Attributes Points in Reaping\n**${prefix}lead sa** : Attributes Points in Salvaging\n**${prefix}lead sc** : Attributes Points in Scavenging\n**${prefix}lead t** : Attributes Points in Taming\n`)
        .addField(`Skills`, `**${prefix}lead m** : Skills Points in Mining\n**${prefix}lead ch** : Skills Points in Chopping\n**${prefix}lead fo** : Skills Points in Foraging\n**${prefix}lead fi** : Skills Points in Fishing\n`)
        .setColor(`BLUE`);
    message.channel.send({embed});
}