// query

const util = require('util');
module.exports = function(bot, message) {
    if (message.author.id == '320933389513523220' || message.author.id == '286737962769580032') {
        try {
            messageContentLengthReduced = message.content.length - 7;
            const result = require('./../functions/database/poolQuery.js')(message.content.substr(7, messageContentLengthReduced), function(err, result) {
                if (err) {
                    message.channel.send(`An error occured.\n\`\`\`xl\n${err}\n\`\`\``);
                } else {
                    message.channel.send(util.inspect(result, false, null), {code:"xl"});
                }
            });
        } catch (err) {
            message.channel.send(`An error occured.\n\`\`\`xl\n${err}\n\`\`\``);
        }
    } else {
        const embed = new Discord.RichEmbed()
            .setAuthor('DiscordRPG Helper', bot.user.avatarURL)
            .setTitle('Developer Command')
            .setDescription(`Sorry, but you are not not verified as a developer of this bot. Only **Ciborn** can use this command.`);
        message.channel.send({embed});
    }
}